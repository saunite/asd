#ac: Autoconnector
================

**asd -> Auto Shell Device

This is a Terminal connection manager (no GUI), intended to be used with 
your favorite Terminal application (Konsole, Gnome-Terminal, yakuake, and 
so on) or to be used in machines with no GUI available.

* Creation date: 2012-09-06
* Last change: 2018-07-23

**Installation Instruction**

For asd to run correctly, some perl modules need to be installed, in Debian
you can just install those packages:

	perl-modules
	libcrypt-ecb-perl
	libexpect-perl
	libterm-readkey-perl
	libcrypt-blowfish-perl
	libio-stty-perl

On Fedora, you should install:

	perl-Expect
	perl-Crypt-ECB
	perl-Crypt-Blowfish
	perl-IO-Stty

In case you are using another system or prefere to install all via CPAN, 
do the following:

Enter cpan:

	$ sudo cpan

Run the installation of the needed modules:

	install Expect
	install Term::ReadKey
	install Getopt::Std
	install Crypt::ECB
	install Crypt::Blowfish
	install IO::Stty

**BASH COMPLETION**

File _bash_completion_asd contains the script for bash completion, it should
be copied as /etc/bash_completion.d/asd or /usr/share/bash-completion/completions 
(or you can just source it when using asd) 

The bash completion will allow you to use tab to complete your saved 
connections, also will try to fill the -P option (Port number) and the
	-t option (type of connection) and to show all available flags / options
